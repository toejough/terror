//+build mage

package main

import (
	"github.com/magefile/mage/sh"
)

// Lint the codebase
func Lint() error {
	return sh.Run("golangci-lint", "run")
}
