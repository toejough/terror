package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	for {
		line, err := reader.ReadString('\n')
		fmt.Fprintf(os.Stderr, "%s", line)
		fmt.Fprintf(os.Stdout, "%s", line)
		if err == io.EOF {
			os.Exit(0)
		}
		if err != nil {
			fmt.Fprintf(os.Stderr, "unable to continue: %v\n", err)
			os.Exit(1)
		}
	}
}
