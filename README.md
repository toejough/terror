# Terror

tee to stderr utility for shell pipelines

## What it does

1. Listens for a line to come in on stdin.
1. Writes it to stdout and to stderr.
1. Repeat.

## How to use it

`<command> | terror | <command that consumes stdin>`

## Example

* Watch the current directory for changes with `fswatch.`.  
* Trigger `go test` on file changes
* See live what file changes are triggering `go test`

`fswatch . | terror | xargs -L1 -I% go test`

## Disclaimer

This utility was written to scratch an itch, and for the moment, it does.  I'm
not sure when or if I'll have the occassion or need to improve it, but I'd love
to hear if you find it useful and would like more features.